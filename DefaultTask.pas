//{$MODE PASCAL}

(* Author: Architector, Sting
 * Company: WaspAce
 * Version: 1.0
 * Name: Default task
 * Description: Task execution with standard settings
 * Minimum WaspAce software version: 3.12.3.1
*)

// Укажите данные параметров настроек задания для проверки работы
// Домен (Пример: waspace.net)
var TaskDomain                                                               := 'mysite.ru';
// URL внешнего источника данных (Пример: http://files.waspace.net/es.txt)
var TaskExternalSourceAddress                                                := 'http://mysite.ru/extsource.txt';
// Маска для клика (Пример: /link?=id)
var TaskMask                                                                 := 'clickmask';
// Время до клика  (Пример: 100)
var TaskTimeBeforeClick                                                      := 10;
// Время после клика (Пример: 100)
var TaskTimeAfterClick                                                       := 10;

{$DEFINE DEBUG}

type
  TWATask = class
  private
    FES: TCSExtSource; 
    FBeforeDepth: Integer;
    FAfterDepth: Integer;
    FBeforeMaskClickInterval: Integer;
    FAfterMaskClickInterval: Integer;
    FDomain: String;
    FPage: String;
    FReferer: String;
    FUserAgent: String;
    
    //Минимальное время нахождения на каждой странице
    FPageMinTime: Integer;
    FTimePerPage: Integer;
    
    FExternalSource: String;
    FTimeBeforeMaskClick: Integer;
    FTimeAfterMaskClick: Integer;
    FMask: String;
    FBottomScrolled: Boolean;
    FPathPoint: Integer;
    FAfterPathPoint: Integer;
    FExSelector: String;
    
    procedure DebugLog(aMessage: String);
    function DefineDepth(aTime: Integer): Integer;
    function GetRandomElement: TElement;
    procedure DoRandomAction(aMaxExecutionTime: Integer);
    procedure GoPath;
    procedure GoAfterPath;
    procedure ClickMask;
  public
    constructor Create;
    procedure Start;
    
    property ExternalSource: String read FExternalSource write FExternalSource;
    property TimeBeforeMaskClick: Integer read FTimeBeforeMaskClick write FTimeBeforeMaskClick;
    property TimeAfterMaskClick: Integer read FTimeAfterMaskClick write FTimeAfterMaskClick;
    property Mask: String read FMask write FMask;
    property Domain: String read FDomain write FDomain;
  end;
  
//..............................................................................TWATask
{Private section}
procedure TWATask.DebugLog(aMessage: String);
begin
  {$IFDEF DEBUG}
  Log(TimeToStr(Now) + #9 + aMessage);
  {$ENDIF}
end;

function TWATask.DefineDepth(aTime: Integer): Integer;
var
  pd: TProbability;
  i: Integer;
const 
  priorities = [1240, 2172, 2523, 1500, 980, 818, 624, 455, 428, 360, 256, 200, 125, 140, 125];
begin
  if FPageMinTime <= aTime then
  begin
    pd := TProbability.Create;
    for i := 1 to 15 do
      if aTime >= FPageMinTime * i  then
        pd.Add(priorities[i - 1], i);    
    Result := pd.Get;
  end
  else
    Result := 1;
end;

function TWATask.GetRandomElement: TElement;
var
  elements: Array of TElement;
  rect: TRect;
  iter: Integer;
begin
  iter := 0;
  rect := TRect.Create(0, 0, 0, 0);
  elements := TABS.ActiveTab.Elements['*:visible'];
  if elements.Length > 0 then
  begin
    result := elements[RandomInt(elements.Length)];
    rect := result.Rect;
  end;
  while ((rect.Top < -2000) or
    (rect.Top > 2000) or
    (elements.Length = 0)) and
    (iter < 10) do
  begin
    elements := TABS.ActiveTab.Elements['*:visible'];
    if elements.Length > 0 then
    begin
      result := elements[RandomInt(elements.Length)];
      rect := result.Rect;
    end;
    inc(iter);
  end;
end;

procedure TWATask.DoRandomAction(aMaxExecutionTime: Integer);
var 
  action: Integer;
  element: TElement;
  rect: TRect;
begin
  while RuningTime < aMaxExecutionTime do
  begin
    action := RandomInt(10);
    case action of
      0:
      begin
        DebugLog(#9'Скроллим до случайного элемента');
        element := GetRandomElement;
        if not element.IsNil then
          element.MouseScrollIntoView;
      end;
      1: 
      begin
        DebugLog(#9'Передвигаем указатель мыши до случайного элемента');
        element := GetRandomElement;
        if not element.IsNil then
          element.MouseMoveTo;
      end;
      2:
      begin
        if not FBottomScrolled then
        begin
          DebugLog(#9'Скроллим страницу до "дна"');
          FBottomScrolled := true;
          element := TABS.ActiveTab.Elements['body'][0];
          rect := element.Rect;
          while (RuningTime < aMaxExecutionTime) and 
            (rect.Bottom > SESSION.BrowserHeight) do
          begin
            TABS.ActiveTab.MouseWheel(0, -120);
            Wait(100 + RandomInt(50));
            element := TABS.ActiveTab.Elements['body'][0];
            rect := element.Rect;
          end;
        end
        else
          Wait(1000);
      end;       
      else
        Wait(1000);
    end;
  end;
end;

procedure TWATask.GoPath;
var
  waited: Integer;
  links: Array of TElement;
  link: TElement;
  pathselector: String;
begin
  FBeforeMaskClickInterval := FTimePerPage - (FTimePerPage div 2) + RandomInt(FTimePerPage);
  if FPathPoint = 0 then
    TABS.ActiveTab.Load(FDomain + FPage, FReferer, false)
  else
  begin
    if FES.Path.Length > 0 then
      pathselector := 
        'a[href^="' + TABS.ActiveTab.Location.Host + '"][href*="' + FES.Path[FPathPoint - 1] + '"]:visible:not(a[href^="//"])' + FExSelector + ', ' + 
        'a[href^="/"][href*="' + FES.Path[FPathPoint - 1] + '"]:visible:not(a[href^="//"])' + FExSelector + ', ' + 
        'a[href^="./"][href*="' + FES.Path[FPathPoint - 1] + '"]:visible:not(a[href^="//"])' + FExSelector + ', ' + 
        'a[href^="../"][href*="' + FES.Path[FPathPoint - 1] + '"]:visible:not(a[href^="//"])' + FExSelector
    else
      pathselector := 'a[href^="' + TABS.ActiveTab.Location.Host + '"]:visible' + FExSelector + ', ' +
        'a[href^="/"]:visible:not(a[href^="//"])' + FExSelector + ', ' +
        'a[href^="./"]:visible:not(a[href^="//"])' + FExSelector + ', ' +
        'a[href^="../"]:visible:not(a[href^="//"])' + FExSelector;
    DebugLog('Домен адресной строки: ' + TABS.ActiveTab.Location.Host);
    DebugLog('Селектор поиска ссылки пути: ' + pathselector);
    links := TABS.ActiveTab.Elements[pathselector, true];
    if links.Length > 0 then
    begin
      link := links[RandomInt(links.Length)];
      link.target := '_top';
      link.MouseClick;
      DebugLog('Переходим по внутренней ссылке: ' + link.href);
    end
    else
      DebugLog('Ссылка пути не найдена');
  end;
  waited := RuningTime;
  DebugLog('Висим на странице ' + IntToStr(FBeforeMaskClickInterval) + ' сек.');
  TABS.ActiveTab.WaitLoaded(1000 * FBeforeMaskClickInterval div 2);
  waited := RuningTime - waited;
  DoRandomAction(RuningTime + FBeforeMaskClickInterval - waited);
  Inc(FPathPoint);
  if (FPathPoint <= FBeforeDepth) and
    (RuningTime < FTimeBeforeMaskClick) then
    GoPath;
end;

procedure TWATask.ClickMask;
var
  attr: String;
  value: String;
  elements: Array of TElement;
  element: TElement;
  maskselector: String;
  waited: Integer;
begin
  FTimePerPage := FTimeAfterMaskClick div (FAfterDepth + 1);
  DebugLog('Среднее время нахождения на странице после клика: ' + IntToStr(FTimePerPage));
  FAfterMaskClickInterval := FTimePerPage - (FTimePerPage div 2) + RandomInt(FTimePerPage);
  if (FMask <> '') and
    (FTimeAfterMaskClick > 0) then
  begin
    value := FMask;
    if Pos('|', value) > 0 then
      attr := CutLeft(value, '|')
    else
      attr := 'href';  
    maskselector := '[' + attr + '*="' + value + '"]:visible' + FExSelector;
    elements := TABS.ActiveTab.Elements[maskselector];
    if elements.Length > 0 then
    begin
      element := elements[RandomInt(elements.Length)];
      DebugLog('Кликаем по элементу, соответствующему маске: ' + element.TagName + ': ' + attr + ' = ' + element.GetAttribute(attr));
      element.MouseClick;
      waited := RuningTime;
      DebugLog('Висим на странице ' + IntToStr(FAfterMaskClickInterval) + ' сек.');
      TABS.ActiveTab.WaitLoaded(1000 * FAfterMaskClickInterval div 2);
      waited := RuningTime - waited;
      DoRandomAction(RuningTime + FAfterMaskClickInterval - waited);
      Inc(FAfterPathPoint);
      if (FAfterPathPoint <= FAfterDepth) and
        (RuningTime < FTimeAfterMaskClick) then
        GoAfterPath;
    end
    else
    begin
      DebugLog('Не найден элемент, соответствующий селектору: ' + maskselector);
      StopExecution;
    end;
  end;  
end;

procedure TWATask.GoAfterPath;
var
  waited: Integer;
  elements: Array of TElement;
  element: TElement;
  attr: String;
  value: String;
  pathselector: String;
begin
  FAfterMaskClickInterval := FTimePerPage - (FTimePerPage div 2) + RandomInt(FTimePerPage);
  if FES.AfterPath.Length > 0 then
    value := FES.AfterPath[FAfterPathPoint]
  else
    value := '';

  if Pos('|', value) > 0 then
    attr := CutLeft(value, '|')
  else
    attr := 'href';
  
  if value <> '' then
    pathselector := '[' + attr + '*="' + value + '"]:visible' + FExSelector
  else
    pathselector := 'a:visible' + FExSelector;
    
  DebugLog('Селектор поиска ссылки пути: ' + pathselector);
  elements := TABS.ActiveTab.Elements[pathselector];
  if elements.Length > 0 then
  begin
    element := elements[RandomInt(elements.Length)];
    element.target := '_top';
    DebugLog('Переходим по элементу пути: ' + element.TagName + ': ' + attr + ' = ' + element.GetAttribute(attr));
    element.MouseClick;
  end  
  else
    DebugLog('Ссылка пути не найдена');
  waited := RuningTime;
  DebugLog('Висим на странице ' + IntToStr(FAfterMaskClickInterval) + ' сек.');
  TABS.ActiveTab.WaitLoaded(1000 * FAfterMaskClickInterval div 2);
  waited := RuningTime - waited;
  DoRandomAction(RuningTime + FAfterMaskClickInterval - waited);
  Inc(FAfterPathPoint);
  if (FAfterPathPoint <= FAfterDepth) and
    (RuningTime < FTimeAfterMaskClick) then
    GoAfterPath;
end;

{Public section}
constructor TWATask.Create;
begin
  TABS.PopupMode := PM_SAMETAB;
  FBottomScrolled := false;
  FPathPoint := 0;
  FAfterPathPoint := 0;
end;

procedure TWATask.Start;
var
  i: Integer;
  tmpattr: String;
  tmpvalue: String;
begin
  //Получаем объект внешнего источника
  FES := GetExtSource(FExternalSource);
  
  FPageMinTime := FES.PageMinTime;
  //Если минимальное время нахождения на странице не указано во внешнем источнике, то ...
  if FPageMinTime < 1 then
    //... назначаем минимальное время равным 10-20 секунд
    FPageMinTime := 10 + RandomInt(11);
  DebugLog('Минимальное время нахождения на странице: ' + IntToStr(FPageMinTime) + ' сек.');
  
  //Если во внешнем источнике не указаны пути по сайту до клика, то ...
  if FES.Path.Count = 0 then
    //... определяем количество внутренних переходов до клика по элементу, соответствующему маске
    FBeforeDepth := DefineDepth(FTimeBeforeMaskClick)
  else
    //в противном случае количество внутренних переходов равно количеству страниц в пути до клика (Path) внешнего источника
    FBeforeDepth := FES.Path.Count;
  DebugLog('Кол-во переходов до клика по элементу: ' + IntToStr(FBeforeDepth));
  
  //Если время после клика больше нуля, то 
  if FTimeAfterMaskClick > 0 then
  begin
    //Если во внешнем источнике не указаны пути по сайту после клика, то ...
    if FES.AfterPath.Count = 0 then
      //... определяем количество внутренних переходов после клика
      FAfterDepth := DefineDepth(FTimeAfterMaskClick)
    else
      //в противном случае количество внутренних переходов равно количеству страниц в пути после клика (AfterPath) внешнего источника
      FAfterDepth := FES.AfterPath.Count;
    DebugLog('Кол-во переходов после клика по элементу: ' + IntToStr(FAfterDepth));
  end;
  
  if FES.Domain <> '' then
    FDomain := FES.Domain;
  if FES.Page <> '' then
    FPage := FES.Page
  else
    FPage := '';
  if FES.Referer <> '' then
    FReferer := FES.Referer
  else
    FReferer := '';
  if FES.UserAgent <> '' then
  begin
    FUserAgent := FES.UserAgent;
    SetUserAgent(FUserAgent);
  end;
  
  //Добавляем и удаляем необходимые фильтры
  DebugLog('Добавляем фильтры по расширению');
  for i := 0 to FES.Extensions.Count - 1 do
  begin
    DebugLog(#9 + FES.Extensions[i]);
    FILTERS.AddExtensionFilter(FES.Extensions[i]);
  end;
  DebugLog('Удаляем фильтры по расширению');
  for i := 0 to FES.ExExtensions.Count - 1 do
  begin
    DebugLog(#9 + FES.ExExtensions[i]);
    FILTERS.DeleteExtensionFilter(FES.ExExtensions[i]);
  end;
  DebugLog('Добавляем MIME фильтры');
  for i := 0 to FES.MimeTypes.Count - 1 do
  begin
    DebugLog(#9 + FES.MimeTypes[i]);
    FILTERS.AddMIMEFilter(FES.MimeTypes[i]);
  end;
  DebugLog('Удаляем MIME фильтры');
  for i := 0 to FES.ExMimeTypes.Count - 1 do
  begin
    DebugLog(#9 + FES.ExMimeTypes[i]);
    FILTERS.DeleteMIMEFilter(FES.ExMimeTypes[i]);
  end;
  DebugLog('Добавляем URL фильтры');
  for i := 0 to FES.URLFilters.Count - 1 do
  begin
    DebugLog(#9 + FES.URLFilters[i]);
    FILTERS.AddURLFilter(FES.URLFilters[i]);
  end;
  DebugLog('Удаляем URL фильтры');
  for i := 0 to FES.ExURLFilters.Count - 1 do
  begin
    DebugLog(#9 + FES.ExURLFilters[i]);
    FILTERS.DeleteURLFilter(FES.ExURLFilters[i]);
  end;
  
  if FES.ExMasks.Count > 0 then
  begin
    for i := 0 to FES.ExMasks.Count - 1 do
    begin
      DebugLog('Исключающая маска: ' + FES.ExMasks[i]);
      tmpvalue := FES.ExMasks[i];
      tmpattr := CutLeft(tmpvalue, '|');
      if tmpvalue = '' then
      begin
        tmpvalue := tmpattr;
        tmpattr := 'href';
      end;
      if i = 0 then
        FExSelector := FExSelector + '[' + tmpattr + '*="' + tmpvalue + '"]'
      else
        FExSelector := FExSelector + ', [' + tmpattr + '*="' + tmpvalue + '"]'
    end;
    FExSelector := ':not(' + FExSelector + ')';
    DebugLog('Исключающий селектор: ' + FExSelector);
  end;
  
  // Устанавливаем свойства объектов браузера
  if FES.Screen <> '' then
  begin
    DebugLog('Установлен объект screen: ' + FES.Screen);
    SESSION.SetScreen(FES.Screen);
  end;
  if FES.Navigator <> '' then
  begin
    DebugLog('Установлен объект navigator: ' + FES.Navigator);
    SESSION.SetNavigator(FES.Navigator);
  end;
  
  FTimePerPage := FTimeBeforeMaskClick div (FBeforeDepth + 1);
  DebugLog('Среднее время нахождения на странице до клика: ' + IntToStr(FTimePerPage));
  GoPath;
  ClickMask;
end;


var task := TWATask.Create;
task.ExternalSource := TaskExternalSourceAddress;
task.TimeBeforeMaskClick := TaskTimeBeforeClick;
task.TimeAfterMaskClick := TaskTimeAfterClick;
task.Mask := TaskMask;
task.Domain := TaskDomain;
task.Start;